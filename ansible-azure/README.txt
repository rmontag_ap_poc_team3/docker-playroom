1. Create VM with Docker installed, e.g. cloudinit-playroom/docker/
2. Login to VM
3. Copy Dockerfile to VM
4. Build Image

docker build -t centos_ansible_powershell .

5. Start container

docker run 
